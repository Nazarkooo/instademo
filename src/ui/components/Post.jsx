import React from "react";
import { AiOutlineHeart, AiFillHeart } from "react-icons/ai";
import { FaRegComment } from "react-icons/fa";
import { FiSend } from "react-icons/fi";
import Avatar from "@mui/material/Avatar";
export default function Post({ postData, setPostFunct }) {
  function setLikeHandler() {
    setPostFunct((prevState) =>
      prevState.map((el) => {
        if (el.id === postData.id) {
          el.is_liked = !el.is_liked;
          return el;
        }
        return el;
      })
    );
  }

  const monthArray = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  console.log(postData);
  return (
    <div key={postData.id} className="post">
      <div className="user__info">
        <div className="user__fio">
          <Avatar src={postData.author.profile_photo_url} />
          <p>{postData.author.username}</p>
        </div>

        <div className="author__fio">
          {`${postData.author.first_name}  ${postData.author.last_name}`}
        </div>
        <div className="author__job">Job: {postData.author.job_title}</div>

        <div className="author__description">{postData.author.description}</div>
        <div className="author__follow-inf">
          <div className="author__followers">
            Followers: {postData.author.followers}
          </div>
          <div className="author__following">
            Following: {postData.author.following}
          </div>
        </div>
      </div>
      <div className="post__photo-wrapper">
        <img src={postData.photos[0].url} alt="" />
      </div>
      <div className="post__btn">
        {postData.is_liked ? (
          <AiFillHeart onClick={setLikeHandler} className="set__like" />
        ) : (
          <AiOutlineHeart onClick={setLikeHandler} className="unset__like" />
        )}

        <FaRegComment />
        <FiSend />
      </div>
      <div className="post__description">
        <p>Count Likes:{postData.likes_count}</p>
        <p>Commnets Count:{postData.comments_count}</p>
        <p>
          Created at:
          {`${new Date(postData.created_at).getDate()}  
            ${monthArray[new Date(postData.created_at).getMonth()]} 
            ${new Date(postData.created_at).getFullYear()} 
          `}
        </p>
      </div>
    </div>
  );
}
