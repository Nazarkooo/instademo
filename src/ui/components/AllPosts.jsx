import React, { useState, useEffect, useRef } from "react";
import axios from "axios";
import Post from "./Post";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

export default function AllPosts() {
  const [defPosts, setDefPosts] = useState([]);
  const [selPosts, setSelPosts] = useState([]);

  const writeIdRef = useRef(null);

  useEffect(() => {
    axios
      .get("https://linkstagram-api.linkupst.com/posts")
      .then(function (response) {
        setDefPosts(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  function writeIdHandler() {
    setSelPosts(
      defPosts.filter((el) => el.id === writeIdRef.current.valueAsNumber)
    );
  }
  function sortLikesHandler() {
    setSelPosts([...defPosts].sort((a, b) => a.likes_count - b.likes_count));
  }
  function sortCommentsHandler() {
    setSelPosts(
      [...defPosts].sort((a, b) => a.comments_count - b.comments_count)
    );
  }

  function sortDateHandler() {
    // 1. конвертувати в timestamp
    // 2. посортувати
    // 3. повернути назад в UTC
    // 4. записати в setSelPosts

    const postsTimestamp = [...defPosts].map((el) => {
      el.created_at = Math.floor(new Date(el.created_at).getTime() / 1000);
      return el;
    });

    postsTimestamp.sort((a, b) => a.created_at - b.created_at);
    console.log(postsTimestamp);
    const postsUTC = postsTimestamp.map((el) => {
      el.created_at = new Date(el.created_at * 1000).toISOString();
      return el;
    });

    setSelPosts(postsUTC);
  }
  return (
    <div className="allposts">
      <div className="navbar">
        <TextField
          ref={writeIdRef}
          id="standard-basic"
          label="Write post ID"
          variant="standard"
        />

        <Button onClick={writeIdHandler} variant="contained" size="small">
          Find post by ID
        </Button>
        <Button onClick={sortLikesHandler} variant="contained" size="small">
          Sort by Likes
        </Button>
        <Button onClick={sortCommentsHandler} variant="contained" size="small">
          Sort by Comments
        </Button>
        <Button onClick={sortDateHandler} variant="contained" size="small">
          Sort by Date
        </Button>
      </div>
      <div className="post__wrapper">
        {selPosts.length !== 0 && (
          <div>
            {selPosts.map((el) => (
              <Post postData={el} setPostFunct={setDefPosts} key={el.id} />
            ))}
          </div>
        )}

        <div>
          {defPosts.map((el) => (
            <Post postData={el} setPostFunct={setDefPosts} key={el.id} />
          ))}
        </div>
      </div>
    </div>
  );
}
