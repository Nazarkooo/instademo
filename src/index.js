import React from "react";
import ReactDOM from "react-dom/client";
import "./ui/style/index.scss";
import AllPosts from "./ui/components/AllPosts";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <AllPosts />
  </React.StrictMode>
);
